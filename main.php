<?php


require_once('./vendor/autoload.php');

use Carbon\Carbon;

$m = new Matcher();

$ynab = new YNABParser('./data/ynab.csv', 'Platinum Credit Card');
$cba = new CBAParser('./data/credit.csv');
$m->run('1st May 2016', $ynab, $cba, '/Users/brad/Desktop/import.csv');

class Matcher
{
    function run($start, $ynab, $cba, $outFile)
    {
        $yt = $ynab->getTransactions($start);
        $ct = $cba->getTransactions($start);

        foreach ($ct as $c) {
            foreach ($yt as $y) {
                if ($y->isMatched()) {
                    continue;
                }
                if ($c->equalsAndSetMatched($y)) {
                    break;
                }
            }
        }
        print ("These transactions are on the Bank Statement but not entered into YNAB.\n----------------------------\n");
        foreach ($ct as $c) {
            if (!$c->isMatched()) {
                print ($c);
            }
        }
        $yi = new YNABImportCreator($outFile);
        if ($yi->generate($ct)) {
            print ("** A CSV file was generated for import into YNAB {$outFile}\n");
        }
        print ( "\n\n");

        print ("These transactions are entered into YNAB but could not be found on Bank Statement.\n----------------------------\n");
        foreach ($yt as $y) {
            if (!$y->isMatched() && $y->isCleared()) {
                print ($y);
            }
        }
    }
}


class YNABImportCreator{


    private $csv;
    public function __construct($outFile)
    {
        $this->csv = \League\Csv\Writer::createFromPath($outFile, "w");

        $header = ['Date','Payee','Category','Memo','Outflow','Inflow'];
        $this->csv->insertOne($header);
    }

    public function generate(array $transactions)
    {
        $count = 0;
        foreach($transactions as $t) {
            if($t->isMatched() === false) {
                $this->insertTransaction($t);
                $count++;
            }
        }
        return $count > 0;
    }

    private function insertTransaction(Transaction $t)
    {
        $data = [
            $t->getDate()->format('d/m/Y'),
            $t->getName(),
            "",
            "",
            $t->isPositive() ? $t->getAmount() : 0,
            $t->isPositive() ? 0 : $t->getAmount() * -1
        ];
        $this->csv->insertOne($data);
    }


}

class YNABCSVRow
{

    private $row;

    function __construct($row)
    {
        $this->row = $row;
        $this->split();
    }

    function getDate()
    {
        return Carbon::createFromFormat("d/m/Y", $this->row[3]);
    }

    function getAmount()
    {
        return (int)(floatval(str_replace("$", "", $this->row[9])) * 100) -
                (int)(floatval(str_replace("$", "", $this->row[10])) * 100);
    }

    function split()
    {
        if (preg_match('/\(Split\ (\d)\/(\d)\)/', $this->row[8], $matches)) {
            $this->split = [(int)$matches[1], (int)$matches[2]];
        } else {
            $this->split = [0, 0];
        }
    }

    function isCleared()
    {
        return $this->row[11] != "U";
    }

    function isSplitStart()
    {
        return ($this->split[0] === 1);
    }

    function isSplitEnd()
    {
        return ($this->split[0] === $this->split[1]);
    }

    function isSplitAppend()
    {
        return ($this->split[0] > 1);
    }

    function getName()
    {
        return $this->row[4];
    }
}

class CBACSVRow
{

    private $row;

    function __construct($row)
    {
        $this->row = $row;
    }

    function getName()
    {
        return $this->row[2];
    }

    function getDate()
    {
        return Carbon::createFromFormat("d/m/Y", $this->row[0]);
    }

    function getAmount()
    {
        return (int)(floatval($this->row[1] * -100));
    }

}

class CBAParser
{

    private $csv;

    function __construct($file)
    {
        $this->csv = \League\Csv\Reader::createFromPath($file);
    }

    function getTransactions($startDate)
    {
        $start = \Carbon\Carbon::parse($startDate);

        $this->csv->addFilter(function ($row) use ($start) {
            return Carbon::createFromFormat('d/m/Y H', $row[0] . " 00")->gte($start);
        });

        $results = $this->csv->fetch(function ($row, $rowOffset) {
            return new CBACSVRow($row);
        });

        $transactions = [];
        foreach ($results as $row) {
            $transactions[] = new Transaction($row->getDate(), $row->getName(), $row->getAmount());
        }
        return array_reverse($transactions);
    }

}

class YNABParser
{

    private $csv;
    private $account;

    function __construct($file, $account)
    {
        $this->csv = \League\Csv\Reader::createFromPath($file);
        $this->account = $account;
    }

    function getTransactions($startDate)
    {
        $start = \Carbon\Carbon::parse($startDate);
        $account = $this->account;
        $this->csv->addFilter(function ($row) use ($start, $account) {
            if ($row[0] !== $account) {
                return false;
            }
            return Carbon::createFromFormat('d/m/Y H', $row[3] . " 00")->gte($start);
        });

        $results = $this->csv->fetch(function ($row) {
            return new YNABCSVRow($row);
        });

        $transactions = [];
        $previous = null;
        foreach ($results as $row) {
            if (!$row->isSplitAppend()) {
                $transaction = new Transaction($row->getDate(), $row->getName(), $row->getAmount(), $row->isCleared());
                $transactions[] = $transaction;
            } else {
                $previous->addAmount($row->getAmount());
            }
            if ($row->isSplitStart()) {
                $previous = $transaction;
            } elseif ($row->isSplitEnd()) {
                $previous = null;
            }
        }
        return $transactions;
    }
}


class Transaction
{
    private $date;
    private $amount;
    private $name;
    private $matched = false;
    private $cleared;

    public function __construct(DateTime $date, string $name, int $amount, $cleared = true)
    {
        $this->name = $name;
        $this->date = $date;
        $this->amount = $amount;
        $this->cleared = $cleared;
    }

    public function isCleared()
    {
        return $this->cleared;
    }

    public function getDate() {
        return $this->date;
    }

    public function getName() {
        return $this->name;
    }

    public function getAmount() {
        return $this->amount / 100;
    }

    public function isPositive() {
        return $this->amount > 0;
    }

    public function equalsAndSetMatched(Transaction $other)
    {
        if ($this->eq($other)) {
            $this->setMatched(true);
            $other->setMatched(true);
            return true;
        }
        return false;
    }

    public function eq(Transaction $other)
    {
        return ( Carbon::parse($other->date)->eq(Carbon::parse($this->date))
            && $other->amount === $this->amount);
    }

    public function setMatched($value = true)
    {
        $this->matched = $value;
    }

    public function isMatched()
    {
        return $this->matched;
    }

    public function addAmount(int $amount)
    {
        $this->amount += $amount;
    }

    public function __toString()
    {
        $date = $this->date->format('Ymd');
        $name = str_pad(substr($this->name, 0, 40), 40, " ", STR_PAD_RIGHT);
        $amount = str_pad("" . number_format($this->amount / 100, 2), 12, " ", STR_PAD_LEFT);
        return $date . "\t" . $name . "\t" . $amount . "\n";
    }

}
